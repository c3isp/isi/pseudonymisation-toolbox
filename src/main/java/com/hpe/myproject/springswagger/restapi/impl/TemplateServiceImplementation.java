/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.hpe.myproject.springswagger.restapi.impl;

import io.swagger.annotations.ApiImplicitParam;

import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;

import com.hpe.myproject.springswagger.restapi.types.Structure;

@ApiModel(value = "Template", description = "Template of REST APIs")
@RestController
@RequestMapping("/v1")
public class TemplateServiceImplementation {
	
	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local
	 * path, it can be in the java classpath or set as an environment variable
	 */
	@Value("${setting1}")
	private String setting1;

	@Value("${setting2}")
	private String setting2;
	
	@Value("${rest.endpoint.url.callGet}")
	private String callGetEndpoint;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;
	
	// encryption values
//	@Value("${encryption.CEFfilein}")
//	private String fileIn;
//	@Value("${encryption.CEFfileout}")
//	private String encCEFfileout;
	@Value("${encryption.Mode}")
	private String encMode;
//	@Value("${encryption.What}")
//	private String encWhat;
	@Value("${encryption.DstkeyPath}")
	private String encDstKeyPath;
//	@Value("${encryption.Srckey}")
//	private String encSrckey;
	
	@Value("${tempfolder.dir}")
	private String tempFolderDir;
	
	@Value("${pyton.path}")
	private String pythonPath;
	@Value("${python.script}")
	private String pythonScript;

	
	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	@Autowired
	private RestTemplate restTemplate;

	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    	return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
    }
	
	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(TemplateServiceImplementation.class);


//	@RequestMapping(method = RequestMethod.POST, value = "/template/{param}/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> create(@RequestHeader(value = "C:/ws/PyJa4/CEF/CEFdata.txt") String headerdata,
//			@RequestHeader(value = "eord") String headerdata2,@RequestHeader(value = "what") String headerdata3, @RequestBody() Structure data) {
//		LOGGER.info("I received a POST request");
//		
//		HttpHeaders responseHeaders = new HttpHeaders();
//		responseHeaders.set("template-header-1", setting1);
//		responseHeaders.set("template-header-2", setting2);
//		ResponseEntity<String> thisresponse = new ResponseEntity<String>(responseHeaders, HttpStatus.OK);
//		FileSystemResource resource = new FileSystemResource(headerdata);
//
//		return thisresponse;
//	}
	//List<String> encList = Arrays.asList(new String[] {"e", "d"});
	
	@ApiOperation(notes = "encrypt or decrypt source and destination IP addresses (preserving the IP address format) from a CEF file using symmetric keys. \n\n"
//			+ " The input is a string JSON representation of either DNF or CNF of search criteria (based on the DPO ontology), and a boolean value, TRUE for long results and FALSE for short results.\n\n"
//
//			+ "", value = "search DPO")
			, value = "Encypt/Decrypt CEF data")
	
	@ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Internal Server Error") 
            })
	@RequestMapping(method=RequestMethod.POST, value="/encryptCEF/")
//    public ResponseEntity<List<String>> updateCEF( @RequestBody @ApiParam(defaultValue = "e", required = true, allowableValues="e, d") String encMode, @ApiParam(defaultValue="hello") String encWhat )

//    public ResponseEntity<List<String>> updateCEF( @RequestBody 
//    		@ApiParam(defaultValue = "e", required = true, allowableValues="e, d", value="Encyption mode. e = encrypt, d = decrypt.") String encMode, 
//    		@ApiParam(defaultValue = "data.txt", required = true, name="fileIn", value="The CEF format input file for the encyption/decryption.") @RequestParam("fileIn")  String fileIn, 
//    		@ApiParam(defaultValue = "encrypted.txt", required = true, name="fileOut", value="The CEF format output file for the encyption/decryption.") @RequestParam("fileOut")  String fileOut, 
//    		@ApiParam(name="encWhat", value="What is to be encrypted/decrypted. s = src, d = dst, b=both.") @RequestParam("encWhat")  String encWhat
//    public ResponseEntity<List<String>> encryptCEF( @RequestBody 
//    		@ApiParam(defaultValue = "e", required = true, 
//    		name="encMode", allowableValues="e, d", value="Encyption mode. e = encrypt, d = decrypt.") @RequestParam("encMode") String encMode, 
//    		@ApiParam(defaultValue = "data.txt", required = true, 
//    		name="fileIn", value="The CEF format input file for the encyption/decryption.") @RequestParam("fileIn")  MultipartFile fileIn,
//    		
//    		@ApiParam(defaultValue = "encrypted.txt", required = true, 
//    		name="fileOut", value="The CEF format output file for the encyption/decryption.") @RequestParam("fileOut")  String fileOut, 
//    		
//    		@ApiParam(name="encWhat", value="What is to be encrypted/decrypted. s = src, d = dst, b=both.") @RequestParam("encWhat")  String encWhat
    public ResponseEntity<List<String>> encryptCEF(  @RequestParam("encMode") String encMode, 
    		 @RequestHeader("bufferLink") String bufferLink,
    		 @RequestHeader(required = false, value = "param", defaultValue="both") String param
    )
	{
		List<String> searchResult = new ArrayList<String>();
        HttpStatus httpstatus;
        ResponseEntity<List<String>> response;
        List<String> errors = new ArrayList<String>();
        //TODO validate the search string
        
 		try {

//        	Path tempFolder = Paths.get(tempFolderDir);
//        	System.out.println(".........tempFolder.toString()......... " + tempFolder.toString());
//        	System.out.println(".........fileIn.getName()......... " + fileIn.getName());
//        	Path tempFile = Paths.get(tempFolder.toString(), fileIn.getName());
//        	System.out.println(".........tempFile......... " + tempFile);
//        	fileIn.transferTo(tempFile.toFile());
 			
 		// Read file from path encDstKeyPath
// 		String encDstKey = new String(Files.readAllBytes(Paths.get(encDstKeyPath)));
 		String encWhat = "b";
 		System.out.println("param received in Java code: " + param);
 		if(param.equals("dst")) {
 			encWhat = "d";
 		} else if(param.equals("src")) {
 			encWhat = "s";
 		}
 		System.out.println("encWhat produced from Java code: " + param);
 		
 		File pythonScriptFile = ResourceUtils.getFile(pythonScript);
 		
 		File encDstKeyFile = ResourceUtils.getFile(encDstKeyPath);
 		String encDstKey = new String(Files.readAllBytes(encDstKeyFile.toPath()));
 		
 		System.out.println("encDstKey is: " + encDstKey);
 		
 		bufferLink = bufferLink.replaceAll("file:///", "/");
 		
 		System.out.println("bufferLink is: " + bufferLink);
 		
 		File folder = new File(bufferLink);
 		File[] listOfFiles = folder.listFiles();
 		String fileName = listOfFiles[0].getName();
 		
 		for (int i = 0; i < listOfFiles.length; i++) {
		  if (listOfFiles[i].isFile()) {
		    System.out.println("File " + listOfFiles[i].getName());
		  } else if (listOfFiles[i].isDirectory()) {
		    System.out.println("Directory " + listOfFiles[i].getName());
		  }
		}
 		
 		System.out.println("fileName is: " + fileName);
 		
 		// encSrcKey is retrieved from another application in the network. Let's assume the key is 0123456789ABCDEF
 		String encSrcKey = "0123456789ABCDEF";
        System.out.println("Python path: " + pythonScriptFile.toPath().toString());
        System.out.println(".........start   process......... " + encWhat + "encMode: " + encMode);
        
    	String[] command = {pythonPath, pythonScriptFile.toPath().toString(), 
		bufferLink + "/" + fileName, encMode, encWhat, encDstKey, encSrcKey};
    	
    	ProcessBuilder probuilder = new ProcessBuilder( command );
    	probuilder.redirectErrorStream(true);
    	//probuilder.redirectOutput(logFile);
    	Process process = probuilder.start();
    	
        BufferedReader bfr = new BufferedReader(new InputStreamReader(process.getInputStream()));

        System.out.println(".........start   process.........");
        String line = "";
        while ((line = bfr.readLine()) != null)
            {
                System.out.println("Python Output: " + line);
            }
        System.out.println("........end   process.......");
        httpstatus = HttpStatus.OK;
    	
        }
        catch (IOException e) { 
        	httpstatus = HttpStatus.BAD_REQUEST;
            System.out.println("IOException caught: " + e); 
            
        	errors.add("something");
        } 
// 	    	finally {}
 
        System.out.println("Goodbye ");
        

//        try {
//        	searchResult = new ArrayList<String>();//dpos.advancedSearchDPO(configuredQuery, longResultFlag);
//        	System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
//    		BufferedReader reader;
//			reader = new BufferedReader(new FileReader(fileIn));
//			String line = reader.readLine();
//			while (line != null) {
//				System.out.println("qqq CEFile: " + line);
//				// read next line
//				line = reader.readLine();
//			}
//			reader.close();
//
//        	httpstatus = HttpStatus.OK;
//        } catch (Exception e) {
//        	// TODO: clean up the way errors are handled.
//        	searchResult = new ArrayList<String>();
//        	searchResult.add("Error running search");
//        	httpstatus = HttpStatus.NOT_FOUND;
//        	e.printStackTrace();
//        }
//    	finally {}
        response = new ResponseEntity<List<String>>(searchResult, httpstatus);
  
//    	httpstatus = HttpStatus.BAD_REQUEST;
//    	// TODO: clean up the way errors are handled.
//    	List<String> errors = new ArrayList<String>();
//    	errors.add("something");
    	response = new ResponseEntity<List<String>>(errors, httpstatus);
       
	   return response;
    
	}

}

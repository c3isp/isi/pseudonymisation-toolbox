/**
Copyright (c) 2019 University of Kent
All rights reserved.

Redistribution and use in source and binary forms, with or
without modification, are permitted provided that the following
conditions are met:

1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
**/
package com.hpe.myproject.springswagger.restapi.types;

public class DmoAttribute {
    private String datatype;
    private String parameterPassingMode;
    private String parameterId;
    private String parameterValue;

    public DmoAttribute(String datatype, String parameterPassingMode, String parameterId, String parameterValue) {
        this.datatype = datatype;
        this.parameterPassingMode = parameterPassingMode;
        this.parameterId = parameterId;
        this.parameterValue = parameterValue;
    }
    
    // Getter Methods

    public String getDatatype() {
        return datatype;
    }

    public String getParameterPassingMode() {
        return parameterPassingMode;
    }

    public String getParameterId() {
        return parameterId;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    // Setter Methods

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public void setParameterPassingMode(String parameterPassingMode) {
        this.parameterPassingMode = parameterPassingMode;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }
    
    @Override
    public String toString() {
        return "DmoAttribute [dataType="
                + datatype
                + ", parameterPassingMode="
                + parameterPassingMode
                + ", parameterId="
                + parameterId
                + ", parameterValue="
                + parameterValue + "]";
    }
}

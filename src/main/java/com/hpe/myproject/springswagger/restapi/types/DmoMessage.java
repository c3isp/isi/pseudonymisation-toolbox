/**
Copyright (c) 2019 University of Kent
All rights reserved.

Redistribution and use in source and binary forms, with or
without modification, are permitted provided that the following
conditions are met:

1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
**/
package com.hpe.myproject.springswagger.restapi.types;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DmoMessage {
    private List<DmoAttribute> Attribute;
    private String actionId;
    private String dataLakeType;
    private String endPoint;
    private String httpMethod;

    public DmoMessage(List<DmoAttribute> Attribute) {
        this.Attribute = Attribute;
    }

    // Getter Methods

    public String getActionId() {
        return actionId;
    }

    public String getDataLakeType() {
        return dataLakeType;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    @JsonProperty("Attribute")
    public List<DmoAttribute> getAttribute() {
        return Attribute;
    }

    // Setter Methods

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public void setDataLakeType(String dataLakeType) {
        this.dataLakeType = dataLakeType;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public void setAttribute(List<DmoAttribute> Attribute) {
        this.Attribute = Attribute;
    }
    
    @Override
    public String toString() {
        return "DmoMessage [actionId="
                + actionId
                + ", dataLakeType="
                + dataLakeType
                + ", endPoint="
                + endPoint
                + ", httpMethod="
                + httpMethod + "]";
    }
}


/**
Copyright (c) 2019 University of Kent
All rights reserved.

Redistribution and use in source and binary forms, with or
without modification, are permitted provided that the following
conditions are met:

1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
**/
package com.hpe.myproject.springswagger;

	import java.io.IOException;
	import java.util.ArrayList;
	import java.util.Arrays;
	import java.util.List;
	import java.util.StringTokenizer;

	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.beans.factory.annotation.Value;
	import org.springframework.http.HttpEntity;
	import org.springframework.http.HttpHeaders;
	import org.springframework.http.HttpMethod;
	import org.springframework.http.HttpStatus;
	import org.springframework.http.MediaType;
	import org.springframework.http.ResponseEntity;
	import org.springframework.scheduling.annotation.EnableScheduling;
	import org.springframework.scheduling.annotation.Scheduled;
	import org.springframework.stereotype.Component;
	import org.springframework.web.client.RestTemplate;

	import com.fasterxml.jackson.databind.JsonNode;
	import com.fasterxml.jackson.databind.ObjectMapper;
	import com.hpe.myproject.springswagger.restapi.types.DmoAttribute;
	import com.hpe.myproject.springswagger.restapi.types.DmoMessage;

	@EnableScheduling
	@Component
	public class RegisterDmoScheduledTask {
	    
	    private final static Logger LOGGER = LoggerFactory.getLogger(RegisterDmoScheduledTask.class);
	    private boolean firstTime = true;
	    
	    /**
	     * Used to call REST endpoints; configured by RestTemplateBuilder
	     */
	    @Autowired
	    private RestTemplate restTemplate;
	    
	    @Value("${dmo.engine.uri}")
	    private String dmoEngineUri;
	    @Value("${dmo.engine.path.listDmo}")
	    private String dmoListPath;
	    @Value("${dmo.engine.path.registerDmo}")
	    private String dmoRegisterPath;
	    @Value("${dmo.engine.path.deleteDmo}")
	    private String dmoDeletePath;
	    
	    private String dmoListUri;
	    private String dmoRegisterUri;
	    private String dmoDeleteUri;
	    
	    @Value("${dmo.psd.fw.actionId}")
	    private String dmoPseudonymiseFirewallActionId;
	    @Value("${dmo.psd.fw.dataLakeType}")
	    private String dmoPseudonymiseFirewallDataLake;
	    @Value("${dmo.psd.fw.endPoint}")
	    private String dmoPseudonymiseFirewallEndPoint;
	    @Value("${dmo.psd.fw.httpMethod}")
	    private String dmoPseudonymiseFirewallHttpMethod;
	    @Value("${dmo.psd.fw.attribute}")
	    private String dmoPseudonymiseFirewallAttribute;
	    
	    @Value("${dmo.depsd.fw.actionId}")
	    private String dmoDePseudonymiseFirewallActionId;
	    @Value("${dmo.depsd.fw.dataLakeType}")
	    private String dmoDePseudonymiseFirewallDataLake;
	    @Value("${dmo.depsd.fw.endPoint}")
	    private String dmoDePseudonymiseFirewallEndPoint;
	    @Value("${dmo.depsd.fw.httpMethod}")
	    private String dmoDePseudonymiseFirewallHttpMethod;
	    @Value("${dmo.depsd.fw.attribute}")
	    private String dmoDePseudonymiseFirewallAttribute;
	    
	    // Start Executing every 5 minutes, after 5 minutes from startup
	    //@Scheduled(initialDelay = 300000, fixedDelay = 300000)
	    @Scheduled(initialDelayString = "${dmo.schedule.initialDelay}", fixedDelayString = "${dmo.schedule.fixedDelay}")
	    public void registerDmoTask() {
	        
	        dmoListUri = dmoEngineUri + dmoListPath;
	        dmoRegisterUri = dmoEngineUri + dmoRegisterPath;
	        dmoDeleteUri = dmoEngineUri + dmoDeletePath;
	        
	        DmoMessage messagePseudonymiseFirewall = new DmoMessage(getDmoAttribute(dmoPseudonymiseFirewallAttribute));
	        messagePseudonymiseFirewall.setActionId(dmoPseudonymiseFirewallActionId);
	        messagePseudonymiseFirewall.setDataLakeType(dmoPseudonymiseFirewallDataLake);
	        messagePseudonymiseFirewall.setEndPoint(dmoPseudonymiseFirewallEndPoint);
	        messagePseudonymiseFirewall.setHttpMethod(dmoPseudonymiseFirewallHttpMethod);
	        
	        DmoMessage messageDePseudonymiseFirewall = new DmoMessage(getDmoAttribute(dmoDePseudonymiseFirewallAttribute));
	        messageDePseudonymiseFirewall.setActionId(dmoDePseudonymiseFirewallActionId);
	        messageDePseudonymiseFirewall.setDataLakeType(dmoDePseudonymiseFirewallDataLake);
	        messageDePseudonymiseFirewall.setEndPoint(dmoDePseudonymiseFirewallEndPoint);
	        messageDePseudonymiseFirewall.setHttpMethod(dmoDePseudonymiseFirewallHttpMethod);
	        
	        List<DmoMessage> messages = Arrays.asList(messagePseudonymiseFirewall, messageDePseudonymiseFirewall);
	        
	        ObjectMapper mapper = new ObjectMapper();
	        
	        int i = 1;
	        for (DmoMessage message : messages) {
	            String dmoName = message.getActionId();
	            String dmoRequest = "";
	            try {
	                dmoRequest = mapper.writeValueAsString(message);
	                //LOGGER.info("dmoRequest="+dmoRequest);
	            } catch (IOException e1) {
	                // TODO Auto-generated catch block
	                e1.printStackTrace();
	            }
	            if (firstTime) {
	                if (++i > messages.size()) firstTime = false; // first time for all DMOs
	                LOGGER.info("Check DMO registration on startup");
	    
	                if (verifyDmoRegistration(dmoName)) {
	                    // delete DMO registration to re-register from scratch because registration parameters might be changed
	                    deleteDmo(dmoRequest);
	                }
	                // register DMO
	                if (registerDmo(dmoRequest)) {
	                    LOGGER.info("DMO "+dmoName+" registered");
	                } else {
	                    LOGGER.warn("DMO "+dmoName+" registration failure on startup");
	                }
	            } else {
	                LOGGER.info("Check DMO registration periodically");
	                if (!verifyDmoRegistration(dmoName)) {
	                    registerDmo(dmoRequest);
	                }
	            }
	            
	        }
	    }

	    /**
	     * @return
	     *      List<DmoAttributes>
	     */
	    private List<DmoAttribute> getDmoAttribute(String dmoAttributeList) {
	        List<DmoAttribute> attribute;
	        attribute = new ArrayList<>();
	        StringTokenizer tokenizer = new StringTokenizer(dmoAttributeList, ",");
	        while (tokenizer.hasMoreElements()) {
	            String t = tokenizer.nextToken();
	            //LOGGER.info("t="+t);
	            String token[] = t.split("[|]");
	            /*for (String temp: token) {
	                LOGGER.info("param="+temp);
	            }*/
	            switch (token.length) {
	                case 4: 
	                    attribute.add(new DmoAttribute(token[0], token[1], token[2], token[3]));
	                    break;
	                case 3:
	                    attribute.add(new DmoAttribute(token[0], token[1], token[2], ""));
	                    break;
	                default:
	                    LOGGER.error("List of attribute parameters not supported");
	            }
	        }
	        return attribute;
	    }

	    private boolean verifyDmoRegistration(String dmoName) {
	        
	        LOGGER.info("Verifying DMO registration for "+dmoName);
	        
	        boolean registered = false;
	        
	        String res;
	        try {
	            res = restTemplate.getForObject(dmoListUri, String.class);
	            if (res.indexOf(dmoName.toLowerCase()) != -1) {
	                registered = true;
	            }
	            LOGGER.info("DMO="+dmoName+", registered="+registered+", available DMOs="+res);
	        } catch (Exception e) {
	            e.printStackTrace();
	            LOGGER.error(e.getMessage());
	            LOGGER.error("Cannot verify DMO registration");
	        }
	        
	        return registered;
	    }
	    
	    private boolean registerDmo(String dmoSpec) {
	        
	        LOGGER.info("Registering DMO with the following parameters="+dmoSpec);
	        
	        boolean success = false;
	        ObjectMapper mapper = new ObjectMapper();
	        
	        JsonNode dmoMessage;
	        try {
	            dmoMessage = mapper.readTree(dmoSpec);
	        } catch (IOException e) {
	            e.printStackTrace();
	            LOGGER.error(e.getMessage());
	            success = false;
	            return success;
	        }

	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<JsonNode> entity = new HttpEntity<>(dmoMessage, headers);
	        
	        ResponseEntity<String> res;
	        try {
	            res = restTemplate.postForEntity(dmoRegisterUri, entity, String.class);
	            if (res.getStatusCode() == HttpStatus.CREATED) {
	                LOGGER.info("DMO registration successful.");
	                success = true;
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            LOGGER.error(e.getMessage());
	            LOGGER.error("DMO registration failure, trying DMO registration again later");
	            success = false;
	        }

	        return success;
	    }
	    
	    private boolean deleteDmo(String dmoSpec) {
	        
	        LOGGER.info("Deleting DMO with the following parameters="+dmoSpec);
	        
	        boolean success = false;
	        
	        /*HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        MultiValueMap<String, String> mapParams = new LinkedMultiValueMap<>();
	        mapParams.add("actionId", dmoName);
	        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(mapParams, headers);*/
	        
	        ObjectMapper mapper = new ObjectMapper();
	        
	        JsonNode dmoMessage;
	        try {
	            dmoMessage = mapper.readTree(dmoSpec);
	        } catch (IOException e) {
	            e.printStackTrace();
	            LOGGER.error(e.getMessage());
	            success = false;
	            return success;
	        }

	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<JsonNode> entity = new HttpEntity<>(dmoMessage, headers);
	        

	        ResponseEntity<String> res;
	        try {
	            res = restTemplate.exchange(dmoDeleteUri, HttpMethod.DELETE, entity, String.class);
	            if (res.getStatusCode() == HttpStatus.OK) {
	                success = true;
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            LOGGER.error(e.getMessage());
	            LOGGER.error("DMO deleting failure");
	            success = false;
	        }
	        
	        return success;
	    }
	    
	}



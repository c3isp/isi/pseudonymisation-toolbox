/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package com.hpe.myproject.springswagger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This class is the main spring boot entry, it implements the initializer class
 * and provides a main method that starts the application
 */
@SpringBootApplication
@EnableSwagger2
public class ApplicationDeployer extends SpringBootServletInitializer {
	
	@Value("${security.activation.status}")
	private boolean securityActivationStatus;
	
	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	
	/**
	 * Spring boot method for configuring the current application, right now it
	 * automatically scans for interfaces annotated via spring boot methods in
	 * all sub classes
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ApplicationDeployer.class);
	}

	/**
	 * Docket is a SwaggerUI configuration component, in particular specifies to
	 * use the V2.0 (SWAGGER_2) of swagger generated interfaces it also tells to
	 * include only paths that are under /v1/. If other rest interfaces are
	 * added with different base path, they won't be included this path selector
	 * can be removed if all interfaces should be documented.
	 */
	@Bean
	public Docket documentation() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.apiInfo(metadata());
		if (!securityActivationStatus) {
			return docket
					.select()
					.paths(PathSelectors.regex("/v1/.*"))
					.build();
		} else {
			return docket
				//.securitySchemes(new ArrayList<ApiKey>(Arrays.asList(new ApiKey("mykey", "api_key", "header"))))
				.securitySchemes(new ArrayList<BasicAuth>(Arrays.asList(new BasicAuth("basicAuth"))))
				.securityContexts(new ArrayList<SecurityContext>(Arrays.asList(securityContext())))
				.select()
				.paths(PathSelectors.regex("/v1/.*"))
				.build();
		}
	}

	/**
	 * Selector for the paths this security context applies to ("/v1/.*")
	 */
	private SecurityContext securityContext() {
		return SecurityContext.builder()
				.securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/v1/.*"))
				.build();
	}
	
	/**
	 * Here we use the same key defined in the security scheme (basicAuth)
	 */
	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		//return new ArrayList<SecurityReference>(Arrays.asList(new SecurityReference("mykey", authorizationScopes)));
		return new ArrayList<SecurityReference>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
	}

	/**
	 * it just tells swagger that no special configuration are requested
	 */
	@Bean
	public UiConfiguration uiConfig() {
		return new UiConfiguration(
                "validatorUrl" // set url
				);
	}

	/**
	 * the metadata are information visualized in the /basepath/swagger-ui.html
	 * interface, only for documentation
	 */
	private ApiInfo metadata() {
		return new ApiInfoBuilder()
				.title("Pseudonymisation Toolbox")
				.description("Encyption and Decryption of CEF files")
				.version("1.0")
				.contact(new Contact("Ed Day", "http://C3ISP.eu/", "ewd5@kent.ac.uk"))
				.build();
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
	    return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
	}


	/**
	 * boot out SpringBoot application
	 */
	public static void main(String[] args) {
		SpringApplication.run(ApplicationDeployer.class, args);
	}

}

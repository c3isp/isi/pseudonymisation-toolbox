#!/usr/bin/env python
import timeit
import struct
import sys
import jks
import pycef
import fileinput

# copy your keys here
##KEY1 = 'some 16-byte key'
##KEY2 = '2nd  16-byte key'
# CEF delimiter character
DELIMITER = ','  

def rotl(b, r):
    return ((b << r) & 0xff) | (b >> (8 - r))

def permute_fwd(state):
    (b0, b1, b2, b3) = state
    b0 += b1
    b2 += b3
    b0 &= 0xff
    b2 &= 0xff
    b1 = rotl(b1, 2)
    b3 = rotl(b3, 5)
    b1 ^= b0
    b3 ^= b2
    b0 = rotl(b0, 4)
    b0 += b3
    b2 += b1
    b0 &= 0xff
    b2 &= 0xff
    b1 = rotl(b1, 3)
    b3 = rotl(b3, 7)
    b1 ^= b2
    b3 ^= b0
    b2 = rotl(b2, 4)
    return (b0, b1, b2, b3)

def permute_bwd(state):
    (b0, b1, b2, b3) = state
    b2 = rotl(b2, 4)
    b1 ^= b2
    b3 ^= b0
    b1 = rotl(b1, 5)
    b3 = rotl(b3, 1)
    b0 -= b3
    b2 -= b1
    b0 &= 0xff
    b2 &= 0xff
    b0 = rotl(b0, 4)
    b1 ^= b0
    b3 ^= b2
    b1 = rotl(b1, 6)
    b3 = rotl(b3, 3)
    b0 -= b1
    b2 -= b3
    b0 &= 0xff
    b2 &= 0xff
    return (b0, b1, b2, b3)


def xor4(x, y):
    return [(x[i] ^ y[i]) & 0xff for i in (0, 1, 2, 3)]


def encrypt(key, ip):
    """16-byte key, ip string like '192.168.1.2'"""
    k = [struct.unpack('<B', x)[0] for x in key]
    try:
        state = [int(x) for x in ip.split('.')]
    except ValueError:
        raise
    try:
        state = xor4(state, k[:4])
        state = permute_fwd(state)
        state = xor4(state, k[4:8])
        state = permute_fwd(state)
        state = xor4(state, k[8:12])
        state = permute_fwd(state)
        state = xor4(state, k[12:16])
    except IndexError:
        raise
    return '.'.join(str(x) for x in state)


def decrypt(key, ip):
    """16-byte key, encrypted ip string like '215.51.199.127'"""
    k = [struct.unpack('<B', x)[0] for x in key]
    try:
        state = [int(x) for x in ip.split('.')]
    except ValueError:
        raise
    try:
        state = xor4(state, k[12:16])
        state = permute_bwd(state)
        state = xor4(state, k[8:12])
        state = permute_bwd(state)
        state = xor4(state, k[4:8])
        state = permute_bwd(state)
        state = xor4(state, k[:4])
    except IndexError:
        raise
    return '.'.join(str(x) for x in state)


def usage():
    print sys.argv[1:]
    print 'usage:  %s CEFfilein CEFfileout e|d s|d|b Srckey '  % sys.argv[0]
    print '\te = encrypt, d = decrypt'
    print '\ts = src, d = dst, b=both'
    print '\ty = skip header'
    print '\tksloc'
    print '\tkspw'
    print '\tSrckey'
    

    sys.exit(0)


def test():
    """basic encryption sanity check"""
    ip = init = '1.2.3.4'
    key = '\xff'*16 
    iterations = 10
    for i in xrange(iterations):
        ip = encrypt(key, ip)
    if ip != '191.207.11.210':
        raise ValueError
    for i in xrange(iterations):
        ip = decrypt(key, ip)
    if ip != init:
        raise ValueError

# ewd this checks valid IPV4 address
def validate_ip(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True
# ewd this checks for 0.0.0.0
def validate_ip_zeros(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i <> 0:
            return False
    return True

def main():

    
##    assert len(KEY1) == 16
##    assert len(KEY2) == 16
    start_time = timeit.default_timer()
    print 'ipcryptCEF.py starting: ', start_time
        
    try:
        test()
    except ValueError:
        print 'test failed'
        sys.exit(0)
    
    try:
        filein = sys.argv[1]
        fileout = sys.argv[2]
        mode = sys.argv[3]
        encrypwhat = sys.argv[4]
        Dstkey = sys.argv[5]
        Srckey= sys.argv[6]
        
        #get the keys
        #ks = jks.KeyStore.load(ksloc, kspw)
	
        #keys = []
        #for alias, sk in ks.secret_keys.items():
        ##    print("Secret key: %s" % sk.alias)
        ##    print("  Algorithm: %s" % sk.algorithm)
        ##    print("  Key size: %d bits" % sk.key_size)
        ##    print("  Key: %s" % "".join("{:02x}".format(b) for b in bytearray(sk.key)))
        ##    print(sk.key)
            #keys.append(sk.key)
        #print keys
            
        #check keys are 16 bytes long
        #for key in keys:
            #assert len(key) == 16

	assert len(Dstkey) == 16
        assert len(Srckey) == 16
	
        
    except:
        usage()

    if mode == 'e':
        process = encrypt
    elif mode == 'd':
        process = decrypt
    else:
        usage()
    if not str(encrypwhat) in ("s", "d", "b"):
        usage()

    encryptsrc=encryptdst=False
    
    CEFout = open(fileout, 'w+')
    f=open(filein)
##    print f
##    print f.read()

##    CEFfile = fileinput.FileInput(f, backup='.bak')

# f.readline()
    for row in f:
        print '0'
        #ewd: skip the header line
        print 'b'
        print 'row', row
        d = pycef.parse(row)
        print 'src: ' , d['src'], 'dst: ' , d['dst']
##            print d 

        if str(encrypwhat) in ("s", "b"):
            print 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
            encryptsrc =  True
        if str(encrypwhat) in ("d", "b"):
            encryptdst =  True
        ip1 = d['src'].strip()
        ip2 = d['dst'].strip()
        #print 'ip1: ', ip1, 'changed to: ', process(keys[0], ip1), 'ip2', ip2, 'changed to: ', process(keys[1], ip2)
        newrow = row
        try:
            print 'c'
            if encryptsrc:
                if not validate_ip_zeros(ip1):
                    if validate_ip(ip1):
                        print 'd'
                        newrow = newrow.replace('src=' + ip1, 'src=' + process(Srckey, ip1))
                        print 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy'

            if encryptdst:
                if not validate_ip_zeros(ip2):
                    if validate_ip(ip2):
                        print 'd'
                        newrow = newrow.replace('dst=' + ip2, 'dst=' + process(Dstkey, ip2))
                  
        except:
            print 'e'
            continue
        print 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'
        print 'mode: ',mode, 'newrow' , newrow

        CEFout.write(newrow)
##    CEFfile.close()
    end_time = timeit.default_timer()
    elapsed = end_time - start_time
    print 'ipcryptCEF.py ending: ', end_time, 'took: ', elapsed

if __name__ == '__main__':
    sys.exit(main())
